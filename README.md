# Enkainos Token #
* Enkainos partnership token smart contract(s)

### How do I get set up? ###
* Configuration
  * Install truffle globally if you don't already have it
    * `npm install -g truffle`
  * Create `.secret` file - add seed phrase (BE CAREFUL!) 
    * TODO: only use `.env`
  * Create a `.env` file - add variables for `MNEMONIC` (again be careful) and `INFURA_PROJECT_ID` 
  * Install dependencies with `npm install` from project directory
* How to run tests
  * `npm run test`
* Deployment instructions
  * `npm run deploy:ENV`

### Contribution guidelines ###
* TDD - skeleton unit tests should be created as a first step
* Code Coverage should be as close to 100% as possible
  * TODO: Solidity code coverage tool?
* PRs with code review and automated unit tests

### Who do I talk to? ###
* Email [TJ Harrison](mailto:tjharrisonjr@gmail.com)

//SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "./IERC884Upgradeable.sol";

/**
 * @title ERC884Upgradeable
 * @notice ERC884 token (ERC20 compatible) built using the OpenZeppelin upgradeable framework
 */
contract ERC884Upgradeable is IERC884Upgradeable, ERC20Upgradeable, AccessControlUpgradeable {

    bytes32 public constant COMPTROLLER = keccak256("COMPTROLLER");

    bytes32 constant private ZERO_BYTES = bytes32(0);
    address constant private ZERO_ADDRESS = address(0);

    mapping(address => bytes32) private verified;
    mapping(address => address) private cancellations; //if an address if cancelled it will point to the next one, 0 address otherwise
    mapping(address => uint256) private holderIndices; //mapping of holder addresses to shareholder number
    mapping(uint256 => address) private shareholders; //list of shareholders - index 0 reserved

    uint256 private numShareholders;

    // @notice Allows verified address or the contract itself
    modifier isVerifiedAddress(address _address) {
        require(verified[_address] != ZERO_BYTES || _address == address(this), "ERC884: Address not verified");
        _;
    }

    modifier isShareholder(address _address) {
        require(holderIndices[_address] != 0, "ERC884: Address not a shareholder");
        _;
    }

    modifier isNotShareholder(address _address) {
        require(holderIndices[_address] == 0, "ERC884: Address is already a shareholder");
        _;
    }

    modifier isNotCancelled(address _address) {
        require(cancellations[_address] == ZERO_ADDRESS, "ERC884: Address has been cancelled");
        _;
    }

    function initialize(string memory _name, string memory _token, address _comptroller) public {
        ERC20Upgradeable.__ERC20_init(_name, _token);

        _setRoleAdmin(COMPTROLLER, COMPTROLLER);
        _setupRole(COMPTROLLER, _comptroller);

        shareholders[0] = address(this); //contract is holder
    }

    function decimals() public view virtual override returns (uint8) {
        return 0;
    }

    /**
     * As each token is minted it is added to the shareholders array.
     * @param _to The address that will receive the minted tokens.
     * @param _amount The amount of tokens to mint.
     */
    function mint(address _to, uint256 _amount)
    public onlyRole(COMPTROLLER) isVerifiedAddress(_to) {
        // if the address does not already own share then
        // add the address to the shareholders array and record the index.
        updateShareholders(_to);
        _mint(_to, _amount);
    }

    /**
     *  The number of addresses that own tokens.
     *  @return the number of unique addresses that own tokens.
     */
    function holderCount()
    public view returns (uint) {
        return numShareholders;
    }

    /**
     *  By counting the number of token holders using `holderCount`
     *  you can retrieve the complete list of token holders, one at a time.
     *  It MUST throw if `index >= holderCount()`.
     *  @param _index The zero-based index of the holder.
     *  @return the address of the token holder with the given index.
     */
    function holderAt(uint256 _index)
    public view
    returns (address) {
        require(shareholders[_index] != address(0), "ERC884: Not a shareholder");
        return shareholders[_index];
    }

    /**
     *  Add a verified address, along with an associated verification hash to the contract.
     *  Upon successful addition of a verified address, the contract must emit
     *  `VerifiedAddressAdded(a, hash, msg.sender)`.
     *  It MUST throw if the supplied address or hash are zero, or if the address has already been supplied.
     *  @param _address The address of the person represented by the supplied hash.
     *  @param _hash A cryptographic hash of the address holder's verified information.
     */
    function addVerified(address _address, bytes32 _hash)
    public isNotCancelled(_address) onlyRole(COMPTROLLER) override {
        require(_address != ZERO_ADDRESS, "ERC884: Cannot verify zero address");
        require(_hash != ZERO_BYTES, "ERC884: Cannot verify empty hash");
        require(verified[_address] == ZERO_BYTES, "ERC884: Address already verified");
        verified[_address] = _hash;
        emit VerifiedAddressAdded(_address, _hash, msg.sender);
    }

    /**
     *  Remove a verified address, and the associated verification hash. If the address is
     *  unknown to the contract then this does nothing. If the address is successfully removed, this
     *  function must emit `VerifiedAddressRemoved(a, msg.sender)`.
     *  It MUST throw if an attempt is made to remove a verifiedAddress that owns Tokens.
     *  @param _address The verified address to be removed.
     */
    function removeVerified(address _address)
    public onlyRole(COMPTROLLER) override {
        require(balanceOf(_address) == 0, "ERC884: Address still holds tokens");
        if (verified[_address] != ZERO_BYTES) {
            verified[_address] = ZERO_BYTES;
            emit VerifiedAddressRemoved(_address, msg.sender);
        }
    }

    /**
     *  Update the hash for a verified address known to the contract.
     *  Upon successful update of a verified address the contract must emit
     *  `VerifiedAddressUpdated(a, oldHash, hash, msg.sender)`.
     *  If the hash is the same as the value already stored then
     *  no `VerifiedAddressUpdated` event is to be emitted.
     *  It MUST throw if the hash is zero, or if the address is unverified.
     *  @param _address The verified address of the person represented by the supplied hash.
     *  @param _hash A new cryptographic hash of the address holder's updated verified information.
     */
    function updateVerified(address _address, bytes32 _hash)
    public isVerifiedAddress(_address) onlyRole(COMPTROLLER) override {
        require(_hash != ZERO_BYTES, "ERC884: Cannot verify empty hash");
        bytes32 oldHash = verified[_address];
        if (oldHash != _hash) {
            verified[_address] = _hash;
            emit VerifiedAddressUpdated(_address, oldHash, _hash, msg.sender);
        }
    }

    /**
     *  Cancel the original address and reissue the Tokens to the replacement address.
     *  Access to this function MUST be strictly controlled.
     *  The `original` address MUST be removed from the set of verified addresses.
     *  Throw if the `original` address supplied is not a shareholder.
     *  Throw if the replacement address is not a verified address.
     *  This function MUST emit the `VerifiedAddressSuperseded` event.
     *  @param _original The address to be superseded. This address MUST NOT be reused.
     *  @param _replacement The address  that supersedes the original. This address MUST be verified.
     */
    function cancelAndReissue(address _original, address _replacement)
    public isShareholder(_original) isNotShareholder(_replacement) isVerifiedAddress(_replacement) onlyRole(COMPTROLLER) override {
        // replace the original address in the shareholders array
        // and update all the associated mappings
        verified[_original] = ZERO_BYTES;
        cancellations[_original] = _replacement;
        uint256 holderIndex = holderIndices[_original];
        shareholders[holderIndex] = _replacement;
        holderIndices[_replacement] = holderIndices[_original];
        holderIndices[_original] = 0;
        transferFrom(_original, _replacement, balanceOf(_original));
        emit VerifiedAddressSuperseded(_original, _replacement, msg.sender);
    }

    /**
     *  The `transfer` function MUST NOT allow transfers to addresses that
     *  have not been verified and added to the contract.
     *  If the `_to` address is not currently a shareholder then it MUST become one.
     *  If the transfer will reduce `msg.sender`'s balance to 0 then that address
     *  MUST be removed from the list of shareholders.
     */
    function transfer(address _to, uint256 _value)
    public isVerifiedAddress(_to) override returns (bool) {
        updateShareholders(_to);
        pruneShareholders(msg.sender, _value);
        return super.transfer(_to, _value);
    }

    /**
     *  The `transferFrom` function MUST NOT allow transfers to addresses that
     *  have not been verified and added to the contract.
     *  If the `_to` address is not currently a shareholder then it MUST become one.
     *  If the transfer will reduce `_from`'s balance to 0 then that address
     *  MUST be removed from the list of shareholders.
     */
    function transferFrom(address _from, address _to, uint256 _value)
    public isVerifiedAddress(_to) override returns (bool) {
        updateShareholders(_to);
        pruneShareholders(_from, _value);
        return super.transferFrom(_from, _to, _value);
    }

    /**
     *  Tests that the supplied address is known to the contract.
     *  @param _address The address to test.
     *  @return true if the address is known to the contract.
     */
    function isVerified(address _address)
    public view override returns (bool) {
        return verified[_address] != ZERO_BYTES;
    }

    /**
     *  Checks to see if the supplied address is a share holder.
     *  @param _address The address to check.
     *  @return true if the supplied address owns a token.
     */
    function isHolder(address _address)
    public view override returns (bool) {
        return holderIndices[_address] != 0;
    }

    /**
     *  Checks that the supplied hash is associated with the given address.
     *  @param _address The address to test.
     *  @param _hash The hash to test.
     *  @return true if the hash matches the one supplied with the address in `addVerified`, or `updateVerified`.
     */
    function hasHash(address _address, bytes32 _hash)
    public view override returns (bool) {
        if (_address == ZERO_ADDRESS) {
            return false;
        }
        return verified[_address] == _hash;
    }

    /**
     *  Checks to see if the supplied address was superseded.
     *  @param _address The address to check.
     *  @return true if the supplied address was superseded by another address.
     */
    function isSuperseded(address _address)
    public view onlyRole(COMPTROLLER) override returns (bool) {
        return cancellations[_address] != ZERO_ADDRESS;
    }

    /**
     *  Gets the most recent address, given a superseded one.
     *  Addresses may be superseded multiple times, so this function needs to
     *  follow the chain of addresses until it reaches the final, verified address.
     *  @param _address The superseded address.
     *  @return the verified address that ultimately holds the share.
     */
    function getCurrentFor(address _address)
    public view isShareholder(msg.sender) override returns (address) {
        return findCurrentFor(_address);
    }

    /**
     *  Recursively find the most recent address given a superseded one.
     *  @param _address The superseded address.
     *  @return the verified address that ultimately holds the share.
     */
    function findCurrentFor(address _address)
    internal view returns (address) {
        address candidate = cancellations[_address];
        if (candidate == ZERO_ADDRESS) {
            return _address;
        }
        return findCurrentFor(candidate);
    }

    /**
     *  If the address is not in the `shareholders` array then push it
     *  and update the `holderIndices` mapping.
     *  @param _address The address to add as a shareholder if it's not already.
     */
    function updateShareholders(address _address) internal {
        if(_address == address(this)){ //No need to count contract as shareholder
            return;
        }

        if (holderIndices[_address] != 0) { //Not a current holder
            return;
        }

        uint256 index = numShareholders + 1; //0 is reserved to indicate
        shareholders[index] = _address;
        holderIndices[_address] = index; //do not subtract 1 to allow it to check against 0 to see if set, account for difference whenever using
        numShareholders++;
    }

    /**
     *  If the address is in the `shareholders` array and the forthcoming
     *  transfer or transferFrom will reduce their balance to 0, then
     *  we need to remove them from the shareholders array.
     *  @param _address The address to prune if their balance will be reduced to 0.
     *  @param _value Number of tokens to remove from the given address
     *  @dev see https://ethereum.stackexchange.com/a/39311
     */
    function pruneShareholders(address _address, uint256 _value)
    internal {
        // Don't need to count contract address
        if(_address == address(this)){
            return;
        }

        //still has shares
        if (balanceOf(_address) - _value > 0) {
            return;
        }

        uint256 holderIndex = holderIndices[_address];
        uint256 lastIndex = numShareholders;
        address lastHolder = shareholders[lastIndex];

        shareholders[holderIndex] = lastHolder; // overwrite the addresses slot with the last shareholder
        holderIndices[lastHolder] = holderIndices[_address]; // update holder indices array
        delete shareholders[numShareholders]; // trim last (moved) entry from shareholders array
        holderIndices[_address] = 0; // and zero out the index for pruned address
    }
}

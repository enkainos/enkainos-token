// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "./ERC884Upgradeable.sol";

contract Enkainos is ERC884Upgradeable {
    function initialize() public {
        ERC884Upgradeable.initialize("Enkainos", "ENK", msg.sender);
    }
}

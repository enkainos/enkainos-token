const { deployProxy } = require('@openzeppelin/truffle-upgrades');
const Enkainos = artifacts.require("Enkainos");

module.exports = async deployer => {
  deployer.deploy(Enkainos);

  const enkainosToken = await deployProxy(Enkainos, [], {deployer});
  console.log(`Deployed Enkainos token at ${enkainosToken.address}`);
};

const Enkainos = artifacts.require("Enkainos");

module.exports = async deployer => {
  const enkainosToken = await Enkainos.deployed();

  await enkainosToken.addVerified('0x78C298B1699BdF644Fa0b426e776E10605Ed0f46', web3.utils.toHex('tj'));
  await enkainosToken.mint('0x78C298B1699BdF644Fa0b426e776E10605Ed0f46', 1);

  await enkainosToken.addVerified('0x7Af7BEBE6e260115Bd73B23cf885654e1246b0D4', web3.utils.toHex('bryanR'));
  await enkainosToken.mint('0x7Af7BEBE6e260115Bd73B23cf885654e1246b0D4', 1);

  await enkainosToken.addVerified('0xfef1D40Bd970dFa3d20d7d3AD38b3fe208327Cdb', web3.utils.toHex('bryanK'));
  await enkainosToken.mint('0xfef1D40Bd970dFa3d20d7d3AD38b3fe208327Cdb', 1);

  await enkainosToken.addVerified('0x51e079804fb8b156F54e4E1852BAE970E771a974', web3.utils.toHex('marie'));
  await enkainosToken.mint('0x51e079804fb8b156F54e4E1852BAE970E771a974', 1);

  console.log('Tokens minted');
};

const Enkainos = artifacts.require("Enkainos");
const {expectRevert} = require("@openzeppelin/test-helpers");

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';

contract('Enkainos', accounts => {

  let enkainosToken;
  const [deployer, p1, p2, p3, p4] = accounts;
  const emptyHash = web3.utils.toHex('');
  
  before(async () => {
    enkainosToken = await Enkainos.deployed();
  });

  context('token contract has just been deployed', () => {
    it('decimals is 0', async () => {
      const decimals = await enkainosToken.decimals();
      assert.equal(decimals.toNumber(), 0);
    });

    it('has comptroller set', async () => {
      const roleAddress = await enkainosToken.COMPTROLLER();
      assert.isTrue(await enkainosToken.hasRole(roleAddress, deployer));
    });

    it('there are no shareholders', async () => {
      assert.equal(await enkainosToken.holderCount(), 0);
    });

    it('the contract itself is registered as a valid token holder', async () => {
      assert.equal(await enkainosToken.holderAt(0), enkainosToken.address, "Contract not valid token holder");
    });
  });

  context('requires valid addresses and hashes', () => {
    it('zero address cannot be holder', async () => {
      assert.isFalse(await enkainosToken.isHolder(ZERO_ADDRESS));
    });

    it('contract does not count as verified', async () => {
      assert.isFalse(await enkainosToken.isVerified(enkainosToken.address));
    });

    it('zero address is not verified', async () => {
      assert.isFalse(await enkainosToken.isVerified(ZERO_ADDRESS));
    });

    it('zero address is not superseded', async () => {
      assert.isFalse(await enkainosToken.isSuperseded(ZERO_ADDRESS));
    });

    it('cannot verify zero address', async () => {
      const hash = web3.utils.toHex('someHash');
      await expectRevert(enkainosToken.addVerified(ZERO_ADDRESS, hash), "ERC884: Cannot verify zero address");
    });

    it('cannot verify with empty hash', async () => {
      await expectRevert(enkainosToken.addVerified(p1, emptyHash), "ERC884: Cannot verify empty hash");
    });

    it('cannot update verification for zero address', async () => {
      const hash = web3.utils.toHex('hashSquared');
      await expectRevert(enkainosToken.updateVerified(ZERO_ADDRESS, hash), "ERC884: Address not verified");
    });
  });

  context('given valid address and hash for partner', () => {
    context('before verification', () => {
      it("won't mint token for unverified address", async () => {
        await expectRevert(enkainosToken.mint(p1, 1), "ERC884: Address not verified");
      });

      it("partner is not already verified", async () => {
        assert.isFalse(await enkainosToken.isVerified(p1));
      });

      it('partner is not already shareholder', async () => {
        assert.isFalse(await enkainosToken.isHolder(p1));
      });

      it('partner is not already superseded', async () => {
        assert.isFalse(await enkainosToken.isSuperseded(p1));
      });

      it("allows un-verifying address that is not verified", async () => {
        await enkainosToken.removeVerified(p3);
      });
    });

    context('potential partner verifies', () => {
      before(async () => {
        await enkainosToken.addVerified(p1, web3.utils.toHex('someHash'));
      });

      it('partner is verified', async () => {
        assert.isTrue(await enkainosToken.isVerified(p1));
      });

      it('partner is not superseded', async () => {
        assert.isFalse(await enkainosToken.isSuperseded(p1));
      });

      it('partner is not shareholder', async () => {
        assert.isFalse(await enkainosToken.isHolder(p1));
      });

      it("won't verify the same address again", async () => {
        const hash = web3.utils.toHex('someHash');
        await expectRevert(enkainosToken.addVerified(p1, hash), "ERC884: Address already verified");
      });
    })

    context("token is minted to shareholder", () => {
      before(async () => {
        await enkainosToken.mint(p1, 1);
      });

      it('sees the number of holders is now 1', async () => {
        assert.equal(await enkainosToken.holderCount(), 1);
      });

      it('finds the first partner correctly', async () => {
        assert.equal(await enkainosToken.holderAt(0), enkainosToken.address);
        assert.equal(await enkainosToken.holderAt(1), p1);
      });

      it('shows the partner is a holder', async () => {
        assert.isTrue(await enkainosToken.isHolder(p1));
      });

      it("can't remove a verified address that holds a token", async () => {
        await expectRevert(enkainosToken.removeVerified(p1), "ERC884: Address still holds tokens");
      });

      it('hash for partner matches', async () => {
        assert.isTrue(await enkainosToken.hasHash(p1, web3.utils.toHex('someHash')));
      });

      it('incorrect hash for partner does not match', async () => {
        assert.isFalse(await enkainosToken.hasHash(p1, web3.utils.toHex('wrongHash')));
      });

      it('empty hash for partner does not match', async () => {
        assert.isFalse(await enkainosToken.hasHash(p1, web3.utils.toHex('')));
      });

      it('correct hash for wrong partner does not match', async () => {
        assert.isFalse(await enkainosToken.hasHash(p2, web3.utils.toHex('someHash')));
      });
    });

    context('partner updates their verification', () => {
      const firstHash = web3.utils.toHex('firstHash')
      const newHash = web3.utils.toHex('newHash')

      before(async () => {
        await enkainosToken.updateVerified(p1, firstHash);
      });

      it('the first hash matches', async () => {
        assert.isTrue(await enkainosToken.hasHash(p1, firstHash));
      });

      it('the new hash does not match', async () => {
        assert.isFalse(await enkainosToken.hasHash(p1, newHash));
      });

      it('allows updating to a new hash', async () => {
        await enkainosToken.updateVerified(p1, newHash);
        assert.isTrue(await enkainosToken.hasHash(p1, newHash));
      });

      it('does not allow updating to an empty hash', async () => {
        await expectRevert(enkainosToken.updateVerified(p1, emptyHash), "ERC884: Cannot verify empty hash");
      });

      it("doesn't emit VerifiedAddressUpdated when given identical hash", async () => {
        await enkainosToken.updateVerified(p1, newHash);
        //TODO: ensure the update event was not emitted
      });
    });
  });

  context('partner is verified', () => {
    const hash = web3.utils.toHex('verificationHash')

    before(async () => {
      await enkainosToken.addVerified(p2, hash);
    });

    it("allows un-verifying the partner without any shares", async () => {
      await enkainosToken.removeVerified(p2);
    });

    it('correctly un-verified the partner', async () => {
      assert.isFalse(await enkainosToken.isVerified(p2));
    });
  });

  context('shareholder wishes to exit partnership', () => {
    const p4Hash = web3.utils.toHex('p4VerificationHash');

    before(async () => {
      await enkainosToken.addVerified(p4, p4Hash);
      await enkainosToken.mint(p4, 1);
    });

    it("allows shareholder to send shares to another shareholder", async () => {
      assert.isTrue(await enkainosToken.isHolder(p1), "Receiving partner not a holder");
      await enkainosToken.transfer(p1, 1, {from: p4});
    });

    it("allows shareholder to send shares to contract", async () => {
      await enkainosToken.transfer(enkainosToken.address, 1, {from: p1});
    });

    it('partner is no longer a holder', async () => {
      assert.isFalse(await enkainosToken.isHolder(p4), "Partner should not be a holder anymore");
    });

    it('ex-shareholder can un-verify their address', async () => {
      await enkainosToken.removeVerified(p4);
    });

    it('correctly un-verified the partner', async () => {
      assert.isFalse(await enkainosToken.isVerified(p2));
    });
  });
});
